$(window).on('load', function () {
    var $preloader = $('#page-preloader'),
        $spinner   = $preloader.find('.spinner');
    $spinner.fadeOut();
    $preloader.delay(350).fadeOut('slow');
});


$(document).ready(function() {

  if ($(".rellax").length)
  {
    var rellax = new Rellax('.rellax',{
      center: true
    });
  };

  $('.studyroom_tabs__select').on('change', function (e) {
    $('.top_tabs__control li a').eq($(this).val()).tab('show'); 
  });

  $('.analytics_tabs__select').on('change', function (e) {
    $('.analytics_nav li a').eq($(this).val()).tab('show'); 
  });

  $('.analytics_card__filter input[type=radio]').click(function () {      
      $(this).tab('show');
  });


  $('.tab_card__button a').click(function() {
    $(this).closest('.investment_card').addClass('tab_card__hover');
  });


  $('.header_mobile__menu').click(function() {
    $('.mobile_menu').slideToggle();
  });

  $('.header_right-block__social__icon.change_lang').click(function() {
    $('.multi_lang').slideToggle();
  });

  /* mobile menu */

  $( ".mobile_menu__accordion" ).accordion({
      collapsible: true,
      active: false,
      heightStyle: "content"
  });

  $('.studyroom_accordion').accordion({
      collapsible: true,
      active: false,
      heightStyle: "content"
  });


  /* Табы бутстрап */

  $('.list_documents__type a').click(function (e) {
    var tab = $(this);
    if(tab.parent('li').hasClass('active')){
        window.setTimeout(function(){
            $(".tab-pane").removeClass('active');
            tab.parent('li').removeClass('active');
        },1);
    }
  });


  $('input[type=file], input[type=checkbox], select').styler();


  $('.investment_card .tab_card__top').matchHeight();
  $('.investment_card .tab_card__financial').matchHeight();
  $('.financial_indicators__card').matchHeight();
  $('.dealings_card').matchHeight();
  $('.autoheight').matchHeight();


  $( ".vacancy_card" ).accordion({
      collapsible: true,
      active: false
    });


  $('.popup-with-move-anim').magnificPopup({
    type: 'inline',
    fixedContentPos: false,
    fixedBgPos: true,
    overflowY: 'auto',
    closeBtnInside: true,
    preloader: false,
    midClick: true,
    removalDelay: 300,
    mainClass: 'my-mfp-slide-bottom'
  });

  $('.popup-with-zoom-anim').magnificPopup({
    type: 'inline',
    fixedContentPos: false,
    fixedBgPos: true,
    overflowY: 'auto',
    closeBtnInside: true,
    preloader: false,    
    midClick: true,
    removalDelay: 300,
    mainClass: 'my-mfp-zoom-in',
    callbacks: {     
      beforeOpen: function() {
        var $triggerEl = $(this.st.el),
            newClass = $triggerEl.data("modal-class");
            if (newClass) {
              this.st.mainClass = this.st.mainClass + ' ' + newClass;
            };
        $('html').css('overflow-y', 'hidden');

        
      },

      close: function(){
        $('html').css('overflow-y', 'auto');
      }
    }
  });

  $('.popup_pages__close').click(function(){
        $('.mfp-close').trigger('click')
    });

  
  

  $('.investment_card .popup-with-move-anim').magnificPopup({
    type: 'inline',
    fixedContentPos: false,
    fixedBgPos: true,
    overflowY: 'auto',
    closeBtnInside: true,
    preloader: false,    
    midClick: true,
    removalDelay: 300,
    mainClass: 'my-mfp-slide-bottom',
    callbacks: {      
      close: function() {
        $('.tab_card__button a').closest('.investment_card').removeClass('tab_card__hover');
      }      
    }
  });

  $(function () {
    $('#tariff .statement').click(function (e) {
        e.preventDefault();
        $('a[href="' + $(this).attr('href') + '"]').tab('show');
    })
  });

  $(function () {
    $('#investment .tab_card__tariff a').click(function (e) {
        e.preventDefault();
        $('a[href="' + $(this).attr('href') + '"]').tab('show');

        $("html, body").animate({
          scrollTop : $("#scroll_services").offset().top
        }, 800);
    })
  });
  

  var mainSlider = new Swiper('.slider .swiper-container', {
    speed: 1000,
    slidesPerView: 1,
    nextButton: '.swiper-button-next',
    prevButton: '.swiper-button-prev',
    pagination: '.swiper-pagination',
    paginationClickable: true, 
    direction: 'vertical',
    effect: 'fade',
    autoplay: 5000,
    loop: true,
    autoplayDisableOnInteraction: false,
    breakpoints:{
      768: {
        direction: 'horizontal'
      }
    }

    
});

  var qtraderSlider = new Swiper('.qtrader-slider .swiper-container', {
    speed: 1000,
    slidesPerView: 1,
    nextButton: '.swiper-button-next',
    prevButton: '.swiper-button-prev',
    pagination: '.swiper-pagination',
    paginationClickable: true, 
    direction: 'vertical',
    effect: 'fade',
    autoplay: 15000,
    loop: true,
    autoplayDisableOnInteraction: false,    
    breakpoints: {    
    768: {
      direction: 'horizontal'
    }
  }
    
});


  var reportSlider = new Swiper('.corporate_report__content', {
    slidesPerView: 1,
    autoHeight: true
    
});

  var galleryThumbs = new Swiper('.corporate_report__years', {
        spaceBetween: 10,
        speed: 800,
        centeredSlides: true,
        slidesPerView: 'auto',
        touchRatio: 0.2,
        slideToClickedSlide: true,
        nextButton: '.swiper-button-next',
        prevButton: '.swiper-button-prev',
        breakpoints: {    
          768: {
            slidesPerView: 1
          }

        }
    });
  reportSlider.params.control = galleryThumbs;
  galleryThumbs.params.control = reportSlider;



  $('.rewards .owl-carousel').owlCarousel({
    
    items: 4,
    margin:35,
    nav:true,
    loop: true,
    navText: [],
    responsiveClass:true,
    responsive:{
        320:{
            items:1,
            nav: false
        },
        768:{
            items:2,
            nav: false
        },
        992:{
            items:3
        },
        1200:{
            items:4
        }
    }
  });


  $( "#layout_home .tab_block" ).tabs({ 
    show: { effect: "fade", duration: 700 } 
  });


  AOS.init({
    offset: 0,
    easing: 'ease-out',
    duration: 1400
  });


  $('.show__search').click(function() {
    $('.header_nav, .header_right-block').hide();
    $('.header_search-block').show()
  });

  $('.header_search-block__close').click(function() {
    $('.header_nav, .header_right-block').show();
    $('.header_search-block').hide()
  });

  $('.scroll_down__icon').click(function() {
    $href = $(this).attr('href');
    $("html, body").animate({
      scrollTop: $($href).offset().top
    }, 700);
    return false;
  });


  $('.popup_pages .scroll_down__icon').click(function() {
    $('.popup_pages').animate({
      scrollTop: 300
    }, 700);
    return false;
  });


  $('.footer .scroll_up').click(function() {
    $("html, body").animate({
      scrollTop : $("#header").offset().top
    }, 1700);
    return false;
  });
  

    $('#layout_team .specialists_cards__item').click(function() {
      
      $.magnificPopup.open({
          items: {
              src: $(this).next("#specialists_popup"),              
          },
          type: 'inline'
      });

      return false;
    });

    /* Api Яндекс карта */
 
    function init(){     
     
        var myMap;
     
        myMap = new ymaps.Map("map", {
            center: [43.228585, 76.950972],
            zoom: 16,
            controls: []
        });

        myMap.behaviors.disable('scrollZoom');
 
        myMap.controls.add("zoomControl", {
            position: {top: 15, left: 15}
        });

        myPlacemark0 = new ymaps.Placemark([43.228585, 76.950972], {}, {
            iconLayout: 'default#image',
            iconImageHref: '/images/icons/logo.png', // картинка иконки
            iconImageSize: [60, 30], // размер иконки
            iconImageOffset: [-60, -30] // позиция иконки
            
        });
        /* Добавляем метки на карту */
        myMap.geoObjects
            .add(myPlacemark0);
     
    };

  if ($("#map").length)
  {
     ymaps.ready(init);
  };

  if ($('input[type="tel"]').length)
  {
    $('input[type="tel"]').inputmask('+7 (999) 999-99-99');  
  };

     

});